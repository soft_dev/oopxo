/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthorn.oopxo;

import java.util.Scanner;

/**
 *
 * @author ACER
 */
public class Game {

    Scanner kb = new Scanner(System.in);
    Player playerX;
    Player playerO;
    Player turn;
    Table table;
    int row, col;

    public Game() {
        playerX = new Player('X');
        playerO = new Player('O');
        table = new Table(playerX, playerO);
    }

    public void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public void showTable() {
        table.showTable();
    }

    public void input() {
        while (true) {
            System.out.println("Please input Row Col :");
            row = kb.nextInt() - 1;
            col = kb.nextInt() - 1;
            if (table.setRowCol(row, col)) {
                break;
            }
            System.out.println("Error : table at row and col is not empty!!!");
        }
    }

    public void showTurn() {
        System.out.println(table.getcurrentPlayer().getName() + " turn");
    }
    public void newGame(){
        table = new Table(playerX,playerO);
    }

    public void run() {
        this.showWelcome();
        while(true){
        this.showTable();
        this.showTurn();
        this.input();
        table.checkWin();
        if(table.isFinish()){
            if(table.getWinner() == null){
                System.out.println("Draw!!!");
            }else{
                System.out.println(table.getWinner().getName()+" Win!!!");
            }
            this.showTable();
            System.out.println("Do you want to start a new game?");
            System.out.println("Yes press 1 /No press 2");
            int num = kb.nextInt();
            if(num == 1){
                newGame();
            }else{
                System.out.println("Bye bye ......");
                break;
            }
        }
        table.switchPlayer();
        }
    }
}
